with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "hepmc-shell";
  buildInputs = [
    python2
    python2Packages.pyhepmc
    python2Packages.graphviz
    graphviz
    swig
    herwig
    lhapdf.pdf_sets.MMHT2014lo68cl
    lhapdf.pdf_sets.MMHT2014nlo68cl
  ];
}
