import hepmc
from graphviz import Digraph

filename = "LHC.hepmc"
filename = "a/RHIC.hepmc"
#filename = "pythia.hepmc"
io = hepmc.IO_GenEvent(filename, "r")

evt = io.get_next_event()

dot = Digraph('G')

dot.body.extend(["rankdir=LR", "ratio=5"])

def obj_id(obj):
    val = repr(obj)
    val = val[val.find("0x")+2:]
    val = val[:val.find(">")]
    val = str(obj.barcode())
    #if isinstance(obj, hepmc.hepmcwrap.GenParticle):
    #    val += " " + str(particle.pdg_id())
    return val

def pid(particle):
    pdg_id = particle.pdg_id()
    pdg_map = {
            2212:   "p+",
            21:     "g",
            22:     "gamma",
            81:     "Cluster",
            82:     "Rem:p+",
            111:    "pi0",
            130:    "K_L0",
            211:    "pi+",
            213:    "rho+",
            310:    "K_S0",
            321:    "K+",
    }
    return pdg_map.get(pdg_id, pdg_map.get(-pdg_id, str(pdg_id)).replace("+", "-"))

def pcolor(particle):
    pdg_id = particle.pdg_id()
    if pdg_id <= 6:
        return "blue"
    elif pdg_id == 21:
        return "red"
    return "black"

def show_prap(eta):
    if abs(eta) < 10:
        return "%.1f" % eta
    elif eta > 0:
        return "+inf"
    else:
        return "-inf"

def show_mom(m):
    return "eta=%s, phi=%.1f, pt=%.0f" % (
        show_prap(m.eta()),
        m.phi(),
        m.perp()
        )

vertices = []

for particle in evt.particles():
#    if particle.pdg_id() != 22:
        vertices.append({ "id" : obj_id(particle), "pid" : pid(particle), "barcode" : particle.barcode(), "phi" : particle.momentum().phi(), "eta" : particle.momentum().eta(), "pt": particle.momentum().perp(), "type" : 1 });
        dot.node(obj_id(particle),
        "%s (%s) %s" % (
            pid(particle),
            show_mom(particle.momentum()),
            particle.barcode(),
            ),
        color = "white" if abs(particle.momentum().eta()) > 1. else pcolor(particle),
        )

def pad(s, l):
    return "0" * (l - len(s)) + s

edges = []

j = 0

for vertex in evt.vertices():
    j += 1
    print vertex
    j += 1
    vertice_id = "node_{}".format(j)
    print vertice_id
    vertices.append({ "id" : vertice_id, "type" : 0 })
    for particle_in in vertex.incoming():
        flow = [particle_in.flow().icode(i) for i in xrange(1, 1+2)]
        flow = filter(lambda c: c != 0, flow)
        edges.append({ "source" : obj_id(particle_in), "target" : vertice_id, "flow" : flow });
    for particle_out in vertex.outgoing():
        flow = [particle_out.flow().icode(i) for i in xrange(1, 1+2)]
        flow = filter(lambda c: c != 0, flow)
        edges.append({ "source" : vertice_id, "target" : obj_id(particle_out), "flow" : flow });

import json
with open("out.json", "w") as fp:
    json.dump({ "vertices" : vertices, "edges" : edges }, fp, indent=2)
